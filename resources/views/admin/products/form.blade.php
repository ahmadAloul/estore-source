@extends('layouts.dashboard')

@section('content')
<form method="POST" action="{{ empty($product) ? route('products.store') : route('products.update', $product) }}" enctype="multipart/form-data">
    @csrf
    @isset($product) @method('PUT') @endisset
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Add new Product</h5>
            <div class="form-group">
                <label>Product name</label>
                <input type="text" class="form-control" name="name" value="{{ old('name', $product->name ?? null) }}">
                @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Category</label>
                <select class="form-control" name="category_id">
                    <option>Select</option>
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}" @isset($product->category_id) {{ $category->id === $product->category_id ? 'selected' : null }} @endisset>{{ $category->name }}</option>
                    @endforeach
                </select>
                @error('category_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Price</label>
                <input type="text" class="form-control" name="price" value="{{ old('price', $product->price ?? null) }}">
                @error('price')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Quantity</label>
                <input type="text" class="form-control" name="quantity" value="{{ old('quantity', $product->quantity ?? null) }}">
                @error('quantity')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" required name="description">{{ old('description', $product->description ?? null) }}</textarea>
                @error('description')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Image</label>
                <input type="file" name ="image" />
            </div>
            <div class="form-group">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="status" value="1" {{ 0 !== old('status', $product->status ?? 0) || !isset($product) ? 'checked' : null }}>
                    <label class="form-check-label">Is published?</label>
                </div>
                @error('status')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="is_featured" value="1" {{ 0 !== old('is_featured', $product->is_featured ?? 0) || !isset($product) ? 'checked' : null }}>
                    <label class="form-check-label">Is featured?</label>
                </div>
                @error('published')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="is_important" value="1" {{ 0 !== old('is_important', $product->is_important ?? 0) || !isset($product) ? 'checked' : null }}>
                    <label class="form-check-label">Is Important?</label>
                </div>
                @error('published')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
    </div>
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Product Gallery</h5>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <input type="file" class="form-control-file" name="gallery[]" multiple>
                        @if($errors->has('gallery.*'))
                            <div class="alert alert-danger">files must be images</div>
                        @endif
                    </div>
                </div>
            </div>
            @if (isset($product->images) && $product->images->count())
            <div class="row">
                @foreach($product->images as $file)
                <div class="col-2 py-2" id="{{ $file->id }}">
                    <div class="card img-card-container">
                        <img src="{{ asset($file->path) }}" class="card-img-top img-thumbnail">
                        <div class="card-img-overlay">
                            <i class="far fa-trash-alt bg-light cursor-pointer" onclick="deleteFile({{ $file->id}}, '{{$file->path }}')"></i>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
        </div>
    </div>
    <div class="main-card mb-3 card">
        <div class="card-body">
            <button type="submit" class="btn btn-success">Submit</button>
        </div>
    </div>
</form>
@endsection

@section('scripts')
<script src="{{ asset('js/sweetalert2.js') }}"></script>
<script src="{{ asset('js/gallery.js') }}" defer></script>
@endsection
