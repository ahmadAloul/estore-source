@extends('layouts.dashboard')

@section('content')

<div class="main-card mb-3 card">
    <div class="card-body">
        <h5 class="card-title">
            Products List
        </h5>
        <div class="card-body">
            <form>
                <div class="input-group">
                    <input type="text" class="form-control" name="term" value="{{ request('term') }}">
                    <div class="input-group-append">
                        <button class="btn btn-success" type="submit"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="main-card mb-3 card">
    <div class="card-body">
        <table class="mb-0 table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Active?</th>
                    <th>Featured?</th>
                    <th>Important?</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Category</th>
                    <th>Image</th>
                    <th>Description</th>
                    <th>Created At</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                    <tr id="model-{{ $product->id }}">
                        <td>{{ $product->name }}</td>
                        <td><span class="badge badge-{{ $product->status ? 'success' : 'warning' }}">&nbsp;{{ $product->status ? 'Yes' : 'No' }}</span></td>
                        <td><span class="badge badge-{{ $product->is_featured ? 'success' : 'warning' }}">&nbsp;{{ $product->is_featured ? 'Yes' : 'No' }}</span></td>
                        <td><span class="badge badge-{{ $product->is_important ? 'success' : 'warning' }}">&nbsp;{{ $product->is_important ? 'Yes' : 'No' }}</span></td>
                        <td>{{ $product->price }}</td>
                        <td>{{ $product->quantity }}</td>
                        <td>{{ optional($product->category)->name }}</td>
                        <td><img src="{{ asset($product->image) }}" width="80px" class="img img-responsive"/></td>
                        <td>{{ substr($product->description, 0, 40) }}</td>
                        <td>{{ $product->created_at }}</td>
                        <td>
                            <a href="{{ route('products.edit', $product) }}" class="font-icon-wrapper font-icon-md">
                                <i class="metismenu-icon pe-7s-pen icon-gradient bg-grow-early"></i>
                            </a>
                            {{-- <a href="javascript:;" class="font-icon-wrapper font-icon-md" onclick="destroy('{{ route('products.destroy', ['id' => $product->id]) }}')">
                                <i class="metismenu-icon pe-7s-trash icon-gradient bg-strong-bliss"></i>
                            </a> --}}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @isset($products)
        <div class="col">
            {{ $products->links() }}
        </div>
        @endisset
    </div>
</div>
@endsection
