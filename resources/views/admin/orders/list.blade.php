@extends('layouts.dashboard')

@section('content')

<div class="main-card mb-3 card">
    <div class="card-body">
        <h5 class="card-title">
            Orders List
        </h5>
        <div class="card-body">
            <form>
                <div class="input-group">
                    <input type="text" class="form-control" name="term" value="{{ request('term') }}">
                    <div class="input-group-append">
                        <button class="btn btn-success" type="submit"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="main-card mb-3 card">
    <div class="card-body">
        <table class="mb-0 table table-striped">
            <thead>
                <tr>
                    <th>Customer Name</th>
                    <th>Phone</th>
                    <th>Reference</th>
                    <th>Total Price</th>
                    <th>Quantity</th>
                    <th>Status</th>
                    <th>Created at</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            
                        
                @foreach($orders as $order)
                    <tr id="model-{{ $order->id }}">
                        <td>{{ $order->customer->name }}</td>
                        <td>{{ $order->customer->phone_number }}</td>
                        <td>{{ $order->reference }}</td>
                        <td>{{ $order->total_price }}</td>
                        <td>
                            @php
                            $count = 0;
                            foreach($order->items as $item) {
                                $count += $item->quantity;
                            }
                            @endphp
                            {{ $count }}
                        </td>
                        <td><span class="badge badge-{{ $order->status == 'new' ? 'warning' : 'success' }}">&nbsp;{{ $order->status }}</span></td>                        
                        <td>{{ $order->created_at }}</td>                        
                        <td>
                            <a href="{{ route('order.items', $order) }}" class="font-icon-wrapper font-icon-md">
                                View Items
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @isset($orders)
        <div class="col">
            {{ $orders->links() }}
        </div>
        @endisset
    </div>
</div>
@endsection
