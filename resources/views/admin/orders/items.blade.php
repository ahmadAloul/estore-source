@extends('layouts.dashboard')

@section('content')

<div class="main-card mb-3 card">
    <div class="card-body">
        <a href="{{route('orders.index')}}" class="btn btn-success pull-right"><- Orders List</a>
        <h5 class="card-title">
            Order Details
        </h5>
        <ul>
            <li><b>Reference:</b> {{$order->reference}}</li>
            <li><b>Total Price:</b> {{$order->total_price}}</li>
            <li><b>Customer Name:</b> {{$order->customer->name}}</li>
            <li><b>Customer Email:</b> {{$order->customer->email}}</li>
            <li><b>Customer Phone:</b> {{$order->customer->phone_number}}</li>
            <li><b>Region:</b> {{$order->customer->addresses[count($order->customer->addresses) - 1]->region}}</li>
            <li><b>City:</b> {{$order->customer->addresses[count($order->customer->addresses) - 1]->city}}</li>
            <li><b>Address 1:</b> {{$order->customer->addresses[count($order->customer->addresses) - 1]->address_1}}</li>
            <li><b>Address 2:</b> {{$order->customer->addresses[count($order->customer->addresses) - 1]->address_2}}</li>
            <li><b>Notes:</b> {{$order->customer->addresses[count($order->customer->addresses) - 1]->notes}}</li>

        </ul>
    </div>
</div>

<div class="main-card mb-3 card">
    <div class="card-body">
        <table class="mb-0 table table-striped">
            <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Quantity</th>
                    <th>Image</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                @foreach($order->items as $item)
                    <tr id="model-{{ $item->id }}">
                        <td>{{ $item->product->name }}</td>
                        <td>{{ $item->quantity }}</td>
                        <td><img src="{{ asset($item->product->image) }}" width="80px" class="img img-responsive"/></td>
                        <td>{{ $item->product->price }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        
    </div>
</div>
@endsection
