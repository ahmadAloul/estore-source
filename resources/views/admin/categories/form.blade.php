@extends('layouts.dashboard')

@section('content')

<div class="main-card mb-3 card">
    <div class="card-body">
        <h5 class="card-title">Add new Category</h5>
        <form method="POST" action="{{ empty($category) ? route('categories.store') : route('categories.update', $category) }}" enctype="multipart/form-data">
            @csrf
            @isset($category) @method('PUT') @endisset
            <div class="form-group">
                <label>Category name</label>
                <input type="text" class="form-control" name="name" value="{{ old('name', $category->name ?? null) }}">
                @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Parent category</label>
                <select class="form-control" name="category_id">
                    <option></option>
                    @foreach ($parentCategories as $parentCategory)
                        <option value="{{ $parentCategory->id }}" @isset($category->parentCategory) {{ $parentCategory->id === $category->parentCategory->id ? 'selected' : null }} @endisset>{{ $parentCategory->name }}</option>
                    @endforeach
                </select>
                @error('category_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" name="description">{{ old('description', $category->description ?? null) }}</textarea>
                @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Image</label>
                <input type="file" name ="image" />
            </div>
            <div class="form-group">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="status" value="1" {{ 0 !== old('published', $category->status ?? 0) || !isset($category) ? 'checked' : null }}>
                    <label class="form-check-label">Is published?</label>
                </div>
                @error('published')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-success">Submit</button>
        </form>
    </div>
</div>

@endsection
