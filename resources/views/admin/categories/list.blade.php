@extends('layouts.dashboard')

@section('content')

<div class="main-card mb-3 card">
    <div class="card-body">
        <h5 class="card-title">
            Categories List
        </h5>
        <div class="card-body">
            <form>
                <div class="input-group">
                    <input type="text" class="form-control" name="term" value="{{ request('term') }}">
                    <div class="input-group-append">
                        <button class="btn btn-success" type="submit"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="main-card mb-3 card">
    <div class="card-body">
        <table class="mb-0 table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Active?</th>
                    <th>Parent Category</th>
                    <th>Image</th>
                    <th>Description</th>
                    <th>Created At</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($categories as $category)
                    <tr id="model-{{ $category->id }}">
                        <td>{{ $category->name }}</td>                        
                        <td><span class="badge badge-{{ $category->status ? 'success' : 'warning' }}">&nbsp;{{ $category->status ? 'Yes' : 'No' }}</span></td>
                        <td>{{ optional($category->parentCategory)->name }}</td>
                        <td><img src="{{ asset($category->image) }}" width="80px" class="img img-responsive"/></td>
                        <td>{{ substr($category->description, 0, 40) }}</td>
                        <td>{{ $category->created_at }}</td>
                        <td>
                            <a href="{{ route('categories.edit', $category) }}" class="font-icon-wrapper font-icon-md">
                                <i class="metismenu-icon pe-7s-pen icon-gradient bg-grow-early"></i>
                            </a>
                            {{-- <a href="javascript:;" class="font-icon-wrapper font-icon-md" onclick="destroy('{{ route('categories.destroy', ['id' => $category->id]) }}')">
                                <i class="metismenu-icon pe-7s-trash icon-gradient bg-strong-bliss"></i>
                            </a> --}}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @isset($categories)
        <div class="col">
            {{ $categories->links() }}
        </div>
        @endisset
    </div>
</div>
@endsection
