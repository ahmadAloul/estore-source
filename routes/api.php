<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['namespace' => 'Api'], function () {
    Route::group(['prefix' => 'customer'], function () {
        Route::post('register', 'CustomersController@register');
        Route::post('login', 'CustomersController@login');
        Route::group(['middleware' => 'auth:customers-api'], function () {
            Route::get('profile', 'CustomersController@show');
            Route::post('profile', 'CustomersController@update');
            Route::apiResource('orders', 'OrderController');
            Route::get('get-addresses', 'CustomerAddressesController@getCustomerAddresses');
            Route::post('store-address', 'CustomerAddressesController@storeCustomerAddress');
            Route::get('order/display/{order}', 'OrderController@orderDetails');
        });
    });

    Route::group(['prefix' => 'category'], function () {
        Route::get('list', 'CategoriesController@index');
        Route::get('{category}/products', 'CategoriesController@relatedProducts');
    });

    Route::group(['prefix' => 'product'], function () {
        Route::get('list', 'ProductsController@index');
        Route::get('display/{product}', 'ProductsController@productDetails');
    });
});
