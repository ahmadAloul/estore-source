<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\OrderItemResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reference' => $this->reference,
            'payment_method' => $this->payment_method,
            'status' => $this->status,
            'total_price' => $this->total_price,
            'created_at' => date($this->created_at),
            'updated_at' => date($this->updated_at),
            'items' => OrderItemResource::collection($this->items),
        ];
    }
}
