<?php

namespace App\Http\Resources;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'facebook_user_id' => $this->facebook_user_id,
            'phone_number' => $this->phone_number,
            'image' => $this->when(isset($this->image), asset($this->image)),
            'api_token' => $this->api_token
        ];
    }
}
