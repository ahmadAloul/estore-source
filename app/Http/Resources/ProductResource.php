<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ProductGalleryResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->when(isset($this->image), asset($this->image)),
            'description' => $this->description,
            'is_featured' => $this->is_featured,
            'is_important' => $this->is_important,
            'price' => $this->price,
            'gallery' => ProductGalleryResource::collection($this->images),
        ];
    }
}
