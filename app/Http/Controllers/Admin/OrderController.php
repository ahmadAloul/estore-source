<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Customer;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $orders = Order::with(['customer', 'items', 'items.product'])->orderBy('id', 'DESC')->paginate(10);
        return view('admin.orders.list', compact('orders'));
    }

    public function items(Order $order) 
    {
        $order = Order::with(['customer', 'customer.addresses', 'items', 'items.product'])->where('id', $order->id)->get();
        $order = $order[0];
        return view('admin.orders.items', compact('order'));
    }
}
