<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;
use App\Models\ProductImage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::when($request->has('term'), function ($query) use ($request) {
            return $query->where('name', 'like', '%' . $request->input('term') . '%');
        })->paginate(10);
        return view('admin.products.list', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('status', 1)->whereNotIn('id', Category::whereNotNull('category_id')->pluck('category_id')->toArray())->get();
        return view('admin.products.form', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->validate([
            'name' => 'required|max:191',
            'status' => 'nullable|boolean',
            'is_featured' => 'nullable|boolean',
            'is_important' => 'nullable|boolean',
            'description' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
            'category_id' => 'required|exists:categories,id',
            'image' => 'required|file|image',
            'gallery.*' => 'file|image',
        ]);
        if ($request->hasFile('image')) {
            $fields['image'] = $request->file('image')->store('uploads/products', 'public');
        }
        $fields['status'] = $fields['status'] ?? 0;
        $fields['is_featured'] = $fields['is_featured'] ?? 0;
        $fields['is_important'] = $fields['is_important'] ?? 0;
        $product = Product::create($fields);
        $product->category()->associate($request->input('category_id'));
        $product->save();
        if (isset($fields['gallery'])) {
            foreach ($fields['gallery'] as $file) {
                $productImages = new ProductImage(['path' => $file->store('uploads/product_images/' . $product->id, 'public'), 'product_id' => $product->id]);
                $productImages->save();
            }
        }
        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::where('status', 1)->whereNotIn('id', Category::whereNotNull('category_id')->pluck('category_id')->toArray())->get();
        return view('admin.products.form', compact('categories', 'product'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $fields = $request->validate([
            'name' => 'required|max:191',
            'status' => 'nullable|boolean',
            'is_featured' => 'nullable|boolean',
            'is_important' => 'nullable|boolean',
            'description' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
            'category_id' => 'required|exists:categories,id',
            'image' => 'nullable|file|image',
            'gallery.*' => 'file|image',
            'deletedFilesIds.*' => 'nullable|integer',
            'deletedFilesPaths.*' => 'nullable|string'
        ]);
        if ($request->hasFile('image')) {
            Storage::disk('public')->delete($product->image);
            $fields['image'] = $request->file('image')->store('uploads/products', 'public');
        }
        if (isset($fields['deletedFilesIds'])) {
            ProductImage::whereIn('id',  $fields['deletedFilesIds'])->delete();
            foreach ($fields['deletedFilesPaths'] as $path) {
                Storage::disk('public')->delete($path);
            }
        }
        if (isset($fields['gallery'])) {
            foreach ($fields['gallery'] as $file) {
                $productImages = new ProductImage(['path' => $file->store('uploads/product_images/' . $product->id, 'public'), 'product_id' => $product->id]);
                $productImages->save();
            }
        }
        $fields['status'] = $fields['status'] ?? 0;
        $fields['is_featured'] = $fields['is_featured'] ?? 0;
        $fields['is_important'] = $fields['is_important'] ?? 0;
        $product->fill($fields);
        $product->category()->associate($request->input('category_id'));
        $product->save();
        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
