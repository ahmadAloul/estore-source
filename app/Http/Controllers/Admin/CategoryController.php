<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::when($request->has('term'), function ($query) use ($request) {
            return $query->where('name', 'like', '%' . $request->input('term') . '%');
        })->paginate(10);
        return view('admin.categories.list', compact('categories'));
    }

    public function create()
    {
        $parentCategories = Category::whereNull('category_id')->get();
        return view('admin.categories.form', compact('parentCategories'));
    }

    public function store(Request $request)
    {
        $fields = $request->validate([
            'name' => 'required|max:191|unique:categories',
            'status' => 'nullable|boolean',
            'image' => 'nullable|file|image',
        ]);
        $fields['description'] = $request->input('description');
        $fields['status'] = $fields['status'] ?? 0;
        if ($request->hasFile('image')) {
            $fields['image'] = $request->file('image')->store('uploads/categories', 'public');
        }
        $category = new Category($fields);
        $category->parentCategory()->associate($request->input('category_id'));
        $category->save();
        return redirect()->route('categories.index');
    }

    public function edit(Category $category)
    {
        $parentCategories = Category::where('id', '<>', $category->id)->whereNull('category_id')->get();
        return view('admin.categories.form', compact('parentCategories', 'category'));
    }

    public function update(Request $request, Category $category)
    {
        $fields = $request->validate([
            'name' => ['required', 'max:191', Rule::unique('categories')->ignore($category->id)],
            'status' => 'nullable|boolean',
            'image' => 'nullable|file|image',
        ]);
        $fields['description'] = $request->input('description');
        $fields['status'] = $fields['status'] ?? 0;
        if ($request->hasFile('image')) {
            Storage::disk('public')->delete($category->image);
            $fields['image'] = $request->file('image')->store('uploads/categories', 'public');
        }
        $category->fill($fields);
        $category->parentCategory()->associate($request->input('category_id'));
        $category->save();
        return redirect()->route('categories.index');
    }
}
