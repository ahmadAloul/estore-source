<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\CategoryResource;
use App\Http\Resources\ProductResource;
use App\Models\Category;
use App\Models\Product;

class CategoriesController extends Controller
{
    public function index()
    {
        // Return all categoties that are not parents
        $category = Category::where('status', 1)->whereNotIn('id', Category::whereNotNull('category_id')->pluck('category_id')->toArray())->get();
        return CategoryResource::collection($category);
    }

    public function relatedProducts($category)
    {
        $products = Product::where('category_id', $category)->where('status', 1)->where('quantity', '>', 0)->get();
        return ProductResource::collection($products);
    }
}
