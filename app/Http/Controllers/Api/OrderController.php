<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Models\Order;
use App\Models\OrderItem;
use App\Http\Resources\OrderResource;
use Illuminate\Validation\Rule;

class OrderController extends Controller
{
    public function index()
    {
        return OrderResource::collection(Auth::user()->orders()->paginate(10));
    }

    public function orderDetails($order)
    {
        $order = Order::with('items')->where('customer_id', Auth::user()->id)->find($order);
        if(is_null($order)) {
            return response(['message' => 'Not Found'], 404);
        }
        return new OrderResource($order);
    }

    public function store(Request $request)
    {
        $input = $request->validate([
            'payment_method' => ['required', 'in:cash,card'],
            'total_price' => 'required|numeric',
            'items' => ['required', 'array'],
            'items.*.product_id' => [
                'required',
                'integer',
                Rule::exists('products', 'id'),
            ],
            'items.*.quantity' => ['required', 'integer']
        ]);
        
        $order = new Order($input);
        $order->reference = (string) Str::uuid();
        $order->customer()->associate(Auth::id());
        $order->status = 'new';
        $order->save();

        foreach ($input['items'] as $attributes) {
            $item = new OrderItem(['quantity' => $attributes['quantity']]);
            $item->order()->associate($order);
            $item->product()->associate($attributes['product_id']);
            $item->save();
        }
        return (new OrderResource($order))->response()->setStatusCode(201);
    }
}
