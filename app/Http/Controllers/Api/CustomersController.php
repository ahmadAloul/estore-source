<?php
namespace App\Http\Controllers\Api;

use Facebook\Facebook;
use GuzzleHttp\Client as HttpClient;
use Firebase\JWT\JWT;
use App\Http\Resources\CustomerResource;
use App\Models\Customer;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    public function register(Request $request)
    {
        $input = $request->validate([
            'name' => ['nullable', 'string', 'max:255'],
            'email' => ['required_without:facebook_access_token', 'required_with:password,name,phone_number', 'string', 'email', 'max:255', 'unique:customers'],
            'password' => ['required_with:email', 'string', 'min:8'],
            'facebook_access_token' => [
                'required_without:email',
                function ($attribute, $value, $fail) {
                    if ($this->verifyFacebookAccessToken($value) === false) {
                        $fail($attribute . ' is invalid.');
                    }
                },
            ],
            'phone_number' => ['required_with:password,name,email', 'unique:customers'],
            // 'firebase_id_token' => [
            //     'required_with:password,name,email,phone_number',
            //     function ($attribute, $value, $fail) use ($request) {
            //         if ($this->verifyFirebaseIdToken($value, $request->phone_number) === false) {
            //             $fail($attribute . ' is invalid.');
            //         }
            //     },
            // ],
        ]);
        $customer = new Customer();
        if ($request->has('email')) {
            $customer->fill([
                'name' => $input['name'],
                'email' => $input['email'],
                'password' => Hash::make($input['password']),
                'phone_number' => $input['phone_number'],
            ]);
        } else {
            $customer->facebook_user_id = $this->getFacebookUserId($input['facebook_access_token']);
        }
        $customer->fill(['api_token' => Str::random(60)])->save();
        return response(new CustomerResource($customer), 201);
    }

    public function login(Request $request)
    {
        if ($request->has(['email', 'password']) && Auth::guard('customers-web')->once($request->only('email', 'password'))) {            
            $customer = Customer::where('email', $request->email)->get();
            return response(new CustomerResource($customer[0]), 201);
        } elseif (
            $request->has('facebook_access_token') &&
            ($customer = Customer::where('facebook_user_id', $this->getFacebookUserId($request->input('facebook_access_token')))->first()) !== null
        ) {
            return new CustomerResource($customer);
        }
        return response(['message' => 'Unauthenticated.'], 401);
    }

    public function show(Request $request)
    {
        return new CustomerResource($request->user());
    }

    public function update(Request $request)
    {
        $customer = Auth::user();
        $input = $request->validate([
            'name' => ['nullable', 'string', 'max:255'],
            'email' => ['nullable', 'string', 'email', 'max:255'],
            'phone_number' => ['nullable'],
            'firebase_id_token' => [
                'required_with:phone_number',
                function ($attribute, $value, $fail) use ($request) {
                    if ($this->verifyFirebaseIdToken($value, $request->phone_number) === false) {
                        $fail($attribute . ' is invalid.');
                    }
                },
            ],
            'image' => ['file', 'image'],
        ]);
        if ($request->hasFile('image')) {
            if (Storage::disk('public')->exists($customer->image)) {
                Storage::disk('public')->delete($customer->image);
            }
            $input['image'] = $request->file('image')->store('images/customers/' . $customer->id, 'public');
        }
        $customer->fill($input)->save();
        return new CustomerResource($customer);
    }

    private function verifyFirebaseIdToken($idToken, $phoneNumber)
    {
        $response = (new HttpClient)->get('https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com');
        if ($response->getStatusCode() == 200) {
            $publicKeys = json_decode($response->getBody(), true);
            do {
                try {
                    $payload = JWT::decode($idToken, current($publicKeys), ['RS256']);
                } catch (\Exception $e) {
                    continue;
                }
            } while (!isset($payload) && next($publicKeys) !== false);
            if (
                isset($payload) &&
                $payload->phone_number == $phoneNumber &&
                time() >= $payload->auth_time &&
                $payload->aud == config('services.firebase.project_id') &&
                $payload->iss == 'https://securetoken.google.com/' . $payload->aud
            ) {
                return true;
            }
        }
        return false;
    }

    private function verifyFacebookAccessToken($accessToken)
    {
        $fb = new Facebook([
            'app_id' => config('services.facebook.app_id'),
            'app_secret' => config('services.facebook.app_secret'),
        ]);
        $tokenMetadata = $fb->getOAuth2Client()->debugToken($accessToken);
        return $tokenMetadata->getIsValid() && Customer::where('facebook_user_id', $tokenMetadata->getUserId())->doesntExist();
    }

    private function getFacebookUserId($accessToken)
    {
        $fb = new Facebook([
            'app_id' => config('services.facebook.app_id'),
            'app_secret' => config('services.facebook.app_secret')
        ]);
        $tokenMetadata = $fb->getOAuth2Client()->debugToken($accessToken);
        return $tokenMetadata->getIsValid() ? $tokenMetadata->getUserId() : null;
    }
}
