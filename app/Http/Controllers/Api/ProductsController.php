<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\ProductResource;
use App\Models\Product;

class ProductsController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::when($request->has('is_featured'), function ($query) use ($request) {
            return $query->where('is_featured', $request->is_featured);
        })->when($request->has('is_important'), function ($query) use ($request) {
            return $query->where('is_important', $request->is_important);
        })->where('status', 1)->where('quantity', '>', 0)->with('images')->latest('id')->limit($request->limit ?? 10)->get();

        return ProductResource::collection($products);
    }

    public function productDetails($product)
    {
        $product = Product::with('images')->find($product);
        if(is_null($product)) {
            return response(['message' => 'Not Found'], 404);
        }
        return new ProductResource($product);
    }
}
