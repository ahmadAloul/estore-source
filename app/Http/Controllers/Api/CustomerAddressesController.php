<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CustomerAddress;
use App\Http\Resources\CustomerAddressResource;
use Illuminate\Support\Facades\Auth;

class CustomerAddressesController extends Controller
{
    public function getCustomerAddresses() {
        return CustomerAddressResource::collection(Auth::guard('customers-api')->user()->addresses()->get());
    }

    public function storeCustomerAddress(Request $request)
    {        
        $input = $request->validate([
            'full_name' => ['required', 'string', 'max:255'],
            'region' =>  ['required', 'string', 'max:255'],
            'address_1' => ['required', 'string'],
            'address_2' => ['nullable', 'string'],
            'city' => ['nullable', 'string', 'max:255'],
            'phone_number' => ['required'],
            'notes' => ['nullable', 'string']
        ]);
        $address = new CustomerAddress($input);
        $customer = Auth::user();
        $address->customer()->associate($customer->id);
        $address->save();
        return (new CustomerAddressResource($address))->response()->setStatusCode(201);
    }
}
