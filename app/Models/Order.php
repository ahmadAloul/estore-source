<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'reference', 'payment_method', 'status', 'total_price', 
    ];
    
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

    public function items()
    {
        return $this->hasMany('App\Models\OrderItem', 'order_id');
    }
}
